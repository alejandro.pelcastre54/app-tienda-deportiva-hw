package com.cursoandoid.tiendadeportiva.ui.view.shop.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.cursoandoid.tiedadeportiva.databinding.FragmentCartBinding
import com.cursoandoid.tiendadeportiva.ui.adapters.CartAdapter
import com.cursoandoid.tiendadeportiva.ui.adapters.ItemCartClickedListener
import com.cursoandoid.tiendadeportiva.ui.viewmodel.ViewModelShop


class CartFragment : Fragment() {

    private val viewModel :ViewModelShop by viewModels()
    private lateinit var adapter: CartAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = FragmentCartBinding.inflate(inflater)
        binding.vmRecyclerCart = viewModel
        binding.lifecycleOwner = this

        adapter = CartAdapter(ItemCartClickedListener {
            findNavController().navigate(CartFragmentDirections.actionCartFragmentToUpdateCartFragment(it))
        })

        viewModel.cartProductShop.observe(viewLifecycleOwner){
            adapter.submitList(it)
        }

        binding.apply {
            binding.rvProductCart.adapter = adapter

        }
        // Inflate the layout for this fragment
        return binding.root
    }

}