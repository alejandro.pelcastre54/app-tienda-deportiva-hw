package com.cursoandoid.tiendadeportiva.ui.view.shop.fragment

import android.app.Application
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.cursoandoid.tiedadeportiva.R
import com.cursoandoid.tiedadeportiva.databinding.FragmentDetailsBinding
import com.cursoandoid.tiedadeportiva.databinding.FragmentUpdateCartBinding
import com.cursoandoid.tiendadeportiva.data.local.entity.ProductsEntity
import com.cursoandoid.tiendadeportiva.model.interfaces.UpdateListener
import com.cursoandoid.tiendadeportiva.ui.viewmodel.ViewModelShop
import com.cursoandoid.tiendadeportiva.ui.viewmodel.ViewModelUpdate
import com.cursoandoid.tiendadeportiva.ui.viewmodelutils.toastFragment
import kotlinx.android.synthetic.*

class UpdateCartFragment(): Fragment(), UpdateListener {

    private val viewModelShop :  ViewModelShop by viewModels()
    private lateinit var viewModel: ViewModelUpdate
    private lateinit var binding : FragmentUpdateCartBinding
    val cart = ObservableField<String>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentUpdateCartBinding.inflate(inflater)
        viewModel = ViewModelProvider(this).get(ViewModelUpdate::class.java)

        binding.vmCart = viewModel
        viewModel.listener = this

        val args = UpdateCartFragmentArgs.fromBundle(requireArguments()).args
        binding.apply {
            titleUpdateCart.setText(args.title)
            descriptionCartUpdate.setText(args.fullDescription)
        }

        return binding.root
    }

    override fun onGetCart(qty: Int) {
        val args = UpdateCartFragmentArgs.fromBundle(requireArguments()).args
        if(qty <= args.stock  && qty != 0){
                var res = args.stock - qty
                val product = ProductsEntity(
                    args.image,
                    args.title,
                    args.description,
                    args.fullDescription,
                    args.id,
                    res,
                    qty,
                    args.price,
                    args.size,
                    args.flag
                )
                viewModelShop.updateShop(product)
            }else{
                toastFragment(getString(R.string.toast_error_qty))

        }
    }

    override fun onDeleteCart() {
        toastFragment(getString(R.string.toast_delete_product))
        val args = UpdateCartFragmentArgs.fromBundle(requireArguments()).args
        args.flag = false

        var res = args.stock + args.cart

        val product = ProductsEntity(
            args.image,
            args.title,
            args.description,
            args.fullDescription,
            args.id,
            res,
            0,
            args.price,
            args.size,
            args.flag
        )
        viewModelShop.updateShop(product)
    }
}