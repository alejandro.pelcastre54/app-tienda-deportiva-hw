package com.cursoandoid.tiendadeportiva.ui.view.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.cursoandoid.tiedadeportiva.R
import com.cursoandoid.tiedadeportiva.databinding.ActivityLoginBinding
import com.cursoandoid.tiendadeportiva.model.interfaces.LoginListener
import com.cursoandoid.tiendadeportiva.ui.utils.Constants
import com.cursoandoid.tiendadeportiva.ui.view.shop.ShopActivity
import com.cursoandoid.tiendadeportiva.ui.viewmodel.ViewModelLogin
import com.cursoandoid.tiendadeportiva.ui.viewmodelutils.toast

class LoginActivity : AppCompatActivity(), LoginListener {

    var constanst = Constants
    private lateinit var binding : ActivityLoginBinding
    private lateinit var viewModel : ViewModelLogin

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        viewModel = ViewModelProvider(this).get(ViewModelLogin::class.java)

        binding.vmLogin = viewModel
        viewModel.listener = this
    }

    override fun onSuccess() {
        val intent = Intent(this, ShopActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun onError(errorType: Int) {
        when(errorType){
            constanst.ERROR_EMPTY_FIELDS -> toast(getString(R.string.txt_login_error_empty_fields))
            constanst.ERROR_EMPTY_EMAIL -> toast(getString(R.string.txt_login_error_empty_email))
            constanst.ERROR_EMPTY_PASSWORD -> toast(getString(R.string.txt_login_error_empty_password))
            constanst.ERROR_CREDENTIALS -> toast(getString(R.string.txt_login_error_invalid_cretentials))
        }
    }
}