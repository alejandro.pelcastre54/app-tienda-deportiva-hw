package com.cursoandoid.tiendadeportiva.ui.viewmodel

import android.app.Application
import android.view.View
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import com.cursoandoid.tiendadeportiva.model.interfaces.UpdateListener

class ViewModelUpdate (application: Application):AndroidViewModel(application) {

    var listener: UpdateListener?= null
    val qty = ObservableField<String>("")

    fun getQty(view:View){
       if (!qty.get().isNullOrEmpty()){
            listener?.onGetCart(qty.get()!!.toInt())
       }
    }

    fun deleteProduct(view:View){
        listener?.onDeleteCart()
    }

}