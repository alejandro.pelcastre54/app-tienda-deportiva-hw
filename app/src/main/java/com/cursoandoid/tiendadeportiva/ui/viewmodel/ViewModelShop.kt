package com.cursoandoid.tiendadeportiva.ui.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.cursoandoid.tiendadeportiva.data.local.db.ShopProductsRoomDatabase
import com.cursoandoid.tiendadeportiva.data.local.entity.ProductsEntity
import com.cursoandoid.tiendadeportiva.data.local.repository.ShopProductsRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ViewModelShop(application: Application) : AndroidViewModel(application) {

    private val productsDao = ShopProductsRoomDatabase.getDatabase(application).shopProductsDao()
    private val repositoryShop: ShopProductsRepository
    val allProductShop: LiveData<List<ProductsEntity>>
    val cartProductShop: LiveData<List<ProductsEntity>>

    init {
        repositoryShop = ShopProductsRepository(productsDao,true)
        allProductShop = repositoryShop.allShopProducts()
        cartProductShop = repositoryShop.oneProduct()
    }

    /* SHOP REPOSITORY */
    fun insertShop(product: ProductsEntity) {
        viewModelScope.launch(Dispatchers.IO) {
            repositoryShop.insertShopProduct(product)
        }
    }

    fun updateShop(product: ProductsEntity){
        viewModelScope.launch (Dispatchers.IO) {
            repositoryShop.updateShopProduct(product)
        }
    }
}
