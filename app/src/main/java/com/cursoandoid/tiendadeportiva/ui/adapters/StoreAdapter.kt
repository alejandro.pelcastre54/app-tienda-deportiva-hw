package com.cursoandoid.tiendadeportiva.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.cursoandoid.tiedadeportiva.databinding.ItemProductBinding
import com.cursoandoid.tiendadeportiva.data.local.entity.ProductsEntity

class StoreAdapters(val listener: ItemStoreClickedListener) : ListAdapter
<ProductsEntity, StoreAdapters.ViewHolder>(ProductsDiffCallBack) {

    companion object ProductsDiffCallBack : DiffUtil.ItemCallback<ProductsEntity>() {
        override fun areItemsTheSame(oldItem: ProductsEntity, newItem: ProductsEntity) =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: ProductsEntity, newItem: ProductsEntity) =
            oldItem == newItem
    }

    class ViewHolder (val binding: ItemProductBinding): RecyclerView.ViewHolder(binding.root){
        fun bind( product: ProductsEntity, clickedListener: ItemStoreClickedListener){
            binding.itemStore = product
            binding.storeClickListener = clickedListener
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemProductBinding.inflate(
            LayoutInflater.from(parent.context),parent,false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item,listener)
    }
}

class ItemStoreClickedListener(val listener: (product: ProductsEntity) -> Unit) {
    fun onClick(product: ProductsEntity) = listener(product)

}