package com.cursoandoid.tiendadeportiva.ui.viewmodel

import android.app.Application
import android.view.View
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import com.cursoandoid.tiendadeportiva.model.interfaces.LoginListener
import com.cursoandoid.tiendadeportiva.ui.utils.Constants

class ViewModelLogin(application: Application) : AndroidViewModel(application) {
    var listener: LoginListener? = null
    val userEmail = ObservableField<String>()
    val userPassword = ObservableField<String>()

    fun validateFields(view: View) {
        if (!userEmail.get().isNullOrEmpty() && !userPassword.get().isNullOrEmpty()) {
            if (userEmail.get().toString() == Constants.USER_NAME &&
                userPassword.get().toString() == Constants.USER_PASSWORD
            ) {
                listener?.onSuccess()
            } else {
                listener?.onError(Constants.ERROR_CREDENTIALS)
            }
        } else {
            if (userEmail.get().isNullOrEmpty() && userPassword.get().isNullOrEmpty()) {
                listener?.onError(Constants.ERROR_EMPTY_FIELDS)
            } else if (userEmail.get().isNullOrEmpty()) {
                listener?.onError(Constants.ERROR_EMPTY_EMAIL)
            } else {
                listener?.onError(Constants.ERROR_EMPTY_PASSWORD)
            }
        }
    }
}