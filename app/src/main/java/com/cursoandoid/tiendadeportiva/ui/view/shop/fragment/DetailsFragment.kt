package com.cursoandoid.tiendadeportiva.ui.view.shop.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.cursoandoid.tiedadeportiva.R
import com.cursoandoid.tiedadeportiva.databinding.FragmentDetailsBinding
import com.cursoandoid.tiendadeportiva.data.local.entity.ProductsEntity
import com.cursoandoid.tiendadeportiva.ui.viewmodel.ViewModelShop
import com.cursoandoid.tiendadeportiva.ui.viewmodelutils.toastFragment

class DetailsFragment : Fragment() {

    private val viewModel :  ViewModelShop by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = FragmentDetailsBinding.inflate(inflater)
        val args = DetailsFragmentArgs.fromBundle(requireArguments()).product

        binding.apply {
            detailsIvProduct.setImageResource(args.image)
            detailsTvTitle.setText(args.title)
            detailsTvDescription.setText(args.fullDescription)
            detailsTvId.setText(args.id.toString())
            detailsTvStock.setText(args.stock.toString())
            detailsTvPrice.setText(args.price.toString())
            detailsTvSize.setText(args.size.toString())

            detailsCartPlus.setOnClickListener {
                if (args.flag == true){
                    toastFragment(getString(R.string.toast_no_add_cart))
                    return@setOnClickListener
                }

                args.flag = true
                val product = ProductsEntity(
                    args.image,
                    args.title,
                    args.description,
                    args.fullDescription,
                    args.id,
                    args.stock,
                    args.cart,
                    args.price,
                    args.size,
                    args.flag
                )
                viewModel.updateShop(product)
                toastFragment(getString(R.string.toast_add_cart))
            }
        }

        return binding.root
    }
}