package com.cursoandoid.tiendadeportiva.ui.view.shop.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.cursoandoid.tiedadeportiva.R
import com.cursoandoid.tiedadeportiva.databinding.FragmentStoreBinding
import com.cursoandoid.tiendadeportiva.data.local.entity.ProductsEntity
import com.cursoandoid.tiendadeportiva.data.local.preferences.ShopPreferences
import com.cursoandoid.tiendadeportiva.ui.adapters.ItemStoreClickedListener
import com.cursoandoid.tiendadeportiva.ui.adapters.StoreAdapters
import com.cursoandoid.tiendadeportiva.ui.utils.Constants
import com.cursoandoid.tiendadeportiva.ui.viewmodel.ViewModelShop

class StoreFragment : Fragment() {


    val item1: ProductsEntity = ProductsEntity(
        R.drawable.soccer,
        "Balón de futbol soccer",
        "Balón 100% de cuero, retro",
        Constants.DESCRIPTION_SOCCER,
        152415,
        40,
        0,
        350,
        5,
        false
    )
    val item2: ProductsEntity = ProductsEntity(
        R.drawable.americano,
        "Balón de futbol americano",
        "Balón tipo ovoide, clásico",
        Constants.DESCRIPTION_AMERICAN,
        125781,
        40,
        0,
        400,
        5,
        false
    )
    val item3: ProductsEntity = ProductsEntity(
        R.drawable.tenis,
        "Pelota de tenis",
        "Pelota profesional para arcilla",
        Constants.DESCRIPTION_TENIS,
        245724,
        60,
        0,
        80,
        3,
        false
    )
    val item4: ProductsEntity = ProductsEntity(
        R.drawable.basketball,
        "Balón de basketball",
        "Balón Profesional, uso rudo",
        Constants.DESCRIPTION_BASKETBALL,
        542360,
        30,
        0,
        320,
        6,
        false
    )
    val item5: ProductsEntity = ProductsEntity(
        R.drawable.volley,
        "Balón de volley ball",
        "Balón para gimnasios",
        Constants.DESCRIPTION_VOLLEYBALL,
        133028,
        30,
        0,
        280,
        4,
        false
    )

    //Preferences
    private lateinit var preferences: ShopPreferences

    // ViewModel
    private val viewModel: ViewModelShop by viewModels()
    private lateinit var adapter: StoreAdapters

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentStoreBinding.inflate(inflater)

        binding.lifecycleOwner = this
        binding.vmRecycler = viewModel

        preferences = ShopPreferences(requireContext())

        if (!preferences.getBoolean(Constants.START)) {
            viewModel.insertShop(item1)
            viewModel.insertShop(item2)
            viewModel.insertShop(item3)
            viewModel.insertShop(item4)
            viewModel.insertShop(item5)
            preferences.putBoolean(Constants.START, true)
        }

        adapter = StoreAdapters(ItemStoreClickedListener {
            findNavController().navigate(
                StoreFragmentDirections
                    .actionStoreFragmentToDetailsFragment(it)
            )
        })

        viewModel.allProductShop.observe(viewLifecycleOwner) {
            adapter.submitList(it)
        }

        binding.apply {
            binding.rvProduct.adapter = adapter

            storeToCart.setOnClickListener {
                findNavController().navigate(R.id.action_store_fragment_to_cartFragment)

            }
        }
        return binding.root
    }
}