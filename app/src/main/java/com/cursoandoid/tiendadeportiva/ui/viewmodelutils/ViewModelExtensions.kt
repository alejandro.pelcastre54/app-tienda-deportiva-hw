package com.cursoandoid.tiendadeportiva.ui.viewmodelutils

import android.content.Context
import android.widget.Toast
import androidx.fragment.app.Fragment

fun Context.toast(message:String){
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun Fragment.toastFragment(message: String) {
    Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
}