package com.cursoandoid.tiendadeportiva.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.cursoandoid.tiedadeportiva.databinding.ItemCartProductBinding
import com.cursoandoid.tiendadeportiva.data.local.entity.ProductsEntity
import com.cursoandoid.tiendadeportiva.ui.viewmodel.ViewModelShop

class CartAdapter(val listener: ItemCartClickedListener):ListAdapter
<ProductsEntity,CartAdapter.ViewHolder>(StoreAdapters.ProductsDiffCallBack){

    companion object ProductsDiffCallBack : DiffUtil.ItemCallback<ProductsEntity>() {
        override fun areItemsTheSame(oldItem: ProductsEntity, newItem: ProductsEntity) =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: ProductsEntity, newItem: ProductsEntity) =
            oldItem == newItem
    }

    class ViewHolder (val binding:ItemCartProductBinding):RecyclerView.ViewHolder(binding.root) {
        fun bind (productsEntity: ProductsEntity, cartClickedListener: ItemCartClickedListener){
                binding.itemCart = productsEntity
                binding.cartClickListener = cartClickedListener
                binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemCartProductBinding.inflate(
            LayoutInflater.from(parent.context),parent,false
        ))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val args = getItem(position)
        if(args.flag) {
            holder.bind(args, listener)
        }
    }
}

class ItemCartClickedListener (val listener:(cartProduct:ProductsEntity)->Unit){
    fun onCartClick(cartProduct: ProductsEntity) =  (listener(cartProduct))
}
