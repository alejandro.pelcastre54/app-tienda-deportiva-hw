package com.cursoandoid.tiendadeportiva.ui.utils

object Constants {
    val ERROR_CREDENTIALS = 1
    val ERROR_EMPTY_EMAIL = 2
    val ERROR_EMPTY_PASSWORD = 3
    val ERROR_EMPTY_FIELDS = 4

    val USER_NAME = "alex@correo.com"
    val USER_PASSWORD = "123"

    val START = "start application"

    val DESCRIPTION_SOCCER = "Toma el control del juego con el Balón Strike de Nike. " +
            "Lleva gráficos de alto contraste para un fácil seguimiento de la pelota, además de unas " +
            "ranuras Nike Aerowtrac y diseño de 12 paneles para un vuelo preciso. Su carcasa texturizada " +
            "proporciona un gran toque y sensación para lograr mejores toques y excelentes movimientos."
    val DESCRIPTION_AMERICAN = "Los balones están en medio de cada pase, cada carrera y" +
            "cada touch down, adquiere este balón para fútbol americano que está elaborado de cámara de " +
            "butilo de alta calidad que brinda una mejor retención de aire y cubierto de material " +
            "sintético para un mejor agarre y durabilidad."
    val DESCRIPTION_TENIS = "Nuestros diseñadores han desarrollado esta pelota de " +
            "competencia para jugadores de tenis que practican en terrenos duros o en tierra batida."
    val DESCRIPTION_BASKETBALL = "Demuestra tus habilidades y domina las canchas de " +
            "básquetbol con el balón Dominate El agarre y su rebote es exepcional con sus canales son " +
            "profundos para un control preciso con la punta del dedo, además de tener una textura que " +
            "facilita su agarre. Es durable e ideal para usar en cualquier tipo de cancha. Se convertirá " +
            "en tu mejor aliado para anotar con facilidad."
    val DESCRIPTION_VOLLEYBALL = "El toque de espuma hace que los intercambios en el vóley " +
            "sean agradables, peso balón ligero, ideal para el aprendizaje del voleibol en interior. " +
            "Resistencia / solidez prueba realizada en laboratorio de 5000 disparos a 50 km/h. Calidad " +
            "de rebote, sin cambios bruscos de dirección cuando el balón toca el suelo."

}