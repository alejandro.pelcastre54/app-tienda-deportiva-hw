package com.cursoandoid.tiendadeportiva.data.local.entity

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "products_table")
data class ProductsEntity(
    @PrimaryKey(autoGenerate = true)
    var image: Int,
    var title: String,
    var description: String,
    var fullDescription: String,
    var id: Int,
    var stock: Int,
    var cart: Int,
    var price: Int,
    var size: Int,
    var flag: Boolean
) : Parcelable