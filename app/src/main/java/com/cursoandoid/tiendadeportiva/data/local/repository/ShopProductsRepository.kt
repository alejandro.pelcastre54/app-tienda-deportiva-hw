package com.cursoandoid.tiendadeportiva.data.local.repository

import androidx.lifecycle.LiveData
import com.cursoandoid.tiendadeportiva.data.local.dao.ProductsDao
import com.cursoandoid.tiendadeportiva.data.local.entity.ProductsEntity

class ShopProductsRepository(val shopProductsDao: ProductsDao,val flagDao:Boolean) {

    fun allShopProducts():LiveData<List<ProductsEntity>> = shopProductsDao.addAllProducts()

    fun oneProduct ():LiveData<List<ProductsEntity>> = shopProductsDao.cartProducts(true)

    suspend fun insertShopProduct(product:ProductsEntity) = shopProductsDao.insertProduct(product)

    suspend fun updateShopProduct(product:ProductsEntity) = shopProductsDao.updateProduct(product)
}
