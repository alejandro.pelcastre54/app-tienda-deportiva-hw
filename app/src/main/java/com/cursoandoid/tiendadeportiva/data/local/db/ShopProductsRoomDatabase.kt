package com.cursoandoid.tiendadeportiva.data.local.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.cursoandoid.tiendadeportiva.data.local.dao.ProductsDao
import com.cursoandoid.tiendadeportiva.data.local.entity.ProductsEntity

@Database(entities = [ProductsEntity::class], version = 1, exportSchema = false)
abstract class ShopProductsRoomDatabase:RoomDatabase() {

    abstract fun shopProductsDao(): ProductsDao

    companion object{
        @Volatile
        private var INSTANCE: ShopProductsRoomDatabase? = null
        fun getDatabase(context: Context):ShopProductsRoomDatabase{
            val tmpInstance = INSTANCE
            if (tmpInstance != null) {
                return tmpInstance
            }
            synchronized(this){
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    ShopProductsRoomDatabase::class.java,
                    "shop_products_database"
                ).fallbackToDestructiveMigration().build()
                INSTANCE = instance
                return instance
            }
        }
    }
}