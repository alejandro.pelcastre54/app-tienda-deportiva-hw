package com.cursoandoid.tiendadeportiva.data.local.dao

import androidx.annotation.RequiresPermission
import androidx.lifecycle.LiveData
import androidx.room.*
import com.cursoandoid.tiendadeportiva.data.local.entity.ProductsEntity

@Dao
interface ProductsDao {
    @Query("SELECT * FROM products_table ORDER BY id ASC")
    fun addAllProducts():LiveData<List<ProductsEntity>>

    @Insert
    fun insertProduct(productsShop:ProductsEntity)

    @Update
    fun updateProduct(productsShop: ProductsEntity)

    @Query("SELECT * FROM products_table WHERE flag = :flag")
    fun cartProducts(flag: Boolean):LiveData<List<ProductsEntity>>

}