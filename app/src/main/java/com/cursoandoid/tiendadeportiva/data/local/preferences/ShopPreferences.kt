package com.cursoandoid.tiendadeportiva.data.local.preferences

import android.content.Context

class ShopPreferences (context: Context) {

    private val sharedPreferences = context.getSharedPreferences("FlagPreference", Context.MODE_PRIVATE)

    fun putBoolean (key: String, flag: Boolean)=
        sharedPreferences.edit().putBoolean(key,flag).apply()

    fun getBoolean(key: String): Boolean {
        return sharedPreferences.getBoolean(key, false)
    }

}