package com.cursoandoid.tiendadeportiva.model.interfaces

interface LoginListener {

    fun onSuccess()
    fun onError(errorType:Int)

}