package com.cursoandoid.tiendadeportiva.model.interfaces

interface UpdateListener {
    fun onGetCart(qty:Int)
    fun onDeleteCart()
}